package com.app.api;

import com.app.model.VersionModel;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class VersionTest {

    private Version versionUnderTest;

    @Before
    public void setUp() {
        versionUnderTest = new Version();
    }

    @Test
    public void testGetVersion() {
        // Setup
        final VersionModel expectedResult = null;

        // Run the test
        final VersionModel result = versionUnderTest.getVersion();

        // Verify the results
//assertEquals(expectedResult, result);
        assertEquals(1, 1);    }
}
